package open.softbox.thrust.samplebitcode01;

public class Library {
    public String getJavaHome() {
        return System.getProperty("java.home");
    }
    public String getOSName() {
        return System.getProperty("os.name");
    }
}
