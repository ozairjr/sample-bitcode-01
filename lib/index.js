const Library = Java.type('open.softbox.thrust.samplebitcode01.Library')
const localLibrary = new Library()
const sum = (x, y) => x + y
const getJavaHome = () => localLibrary.getJavaHome()

exports = {
  sum,
  getJavaHome
}
