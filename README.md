# sample-bitcode-01

Um pequeno exemplo de um bitcode para o Thrust.

## v0.0.1

* Primeira versão.
* Apenas função `sum`.

## v0.0.2

* Segunda versão.
* Adicionada função `getJavaHome` em *index.js*.
* Adicionado um **.jar** local.
